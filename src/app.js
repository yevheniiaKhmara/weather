// Components

import { observer } from 'mobx-react-lite';
import { useEffect } from 'react';
import { CurrentWeather } from './components/CurrentWeather';
import { Filter } from './components/Filter';
import { Forecast } from './components/Forecast';
import { Head } from './components/Head';
import { useStore } from './hooks/useStore';
import { useWeather } from './hooks/useWeather';


// Instruments


export const App = observer(() => {
    return (
        <main>
            <Head />
            <Filter />
            <CurrentWeather />
            <Forecast />
        </main>
    );
});

