import { observer } from 'mobx-react-lite';
import { useEffect, useState } from 'react';
import { useStore } from '../../hooks/useStore';
import { useWeather } from '../../hooks/useWeather';
import { Day } from '../Day';

export const Forecast = observer(() => {
    const { data, isFetched } = useWeather();
    const store = useStore();
    const [weaterList, setWeatherList] = useState([]);

    useEffect(() => {
        if (isFetched) {
            const weather = store.isFiltered ? store.filteredDays(data) : data;
            setWeatherList(weather);
            store.setSelectedDayId(weather[ 0 ]?.id || '');
        }
    }, [store.isFiltered, isFetched]);

    const forecastJSX = isFetched && weaterList.slice(0, 7).map((weather) => {
        return (
            <Day
                key = { weather.id }
                { ...weather } />
        );
    });

    return (
        <div className = 'forecast'>
            { store.selectedDayId ? forecastJSX : <p className = 'message'>По заданным критериям нет доступных дней!</p> }
        </div>
    );
});
