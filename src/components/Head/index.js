import { format } from 'date-fns';
import { ru } from 'date-fns/locale';
import { observer } from 'mobx-react-lite';
import { useEffect, useState } from 'react';
import { useStore } from '../../hooks/useStore';
import { useWeather } from '../../hooks/useWeather';

export const Head = observer(() => {
    const { data, isFetched } = useWeather();
    const store = useStore();
    const [currentDay, setCurrentDay] = useState();

    useEffect(() => {
        if (store.selectedDayId) {
            const selectedDay = isFetched && data.find((day) => day.id === store.selectedDayId);
            setCurrentDay(selectedDay);
        }
    }, [store.selectedDayId]);

    return (
        <>
            {
                store.selectedDayId
                    ?  <div className = 'head'>
                        <div className = { `icon ${currentDay?.type}` }></div>
                        <div className = 'current-date'>
                            <p>{ currentDay && format(new Date(currentDay?.day), 'eeee', { locale: ru }) }</p>
                            <span>{ currentDay && format(new Date(currentDay?.day), 'dd MMMM', { locale: ru }) }</span>
                        </div>
                    </div>
                    : ''
            }
        </>
    );
});
