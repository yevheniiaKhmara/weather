import { observer } from 'mobx-react-lite';
import { useStore } from '../../hooks/useStore';

export const Filter = observer(() => {
    const store = useStore();
    const {
        type,
        minTemperature,
        maxTemperature,
        isFiltered,
        isFormBlocked,
    } = store;


    return (
        <div className = 'filter'>
            <span
                className = { `checkbox ${type === 'cloudy' ? 'selected' : ''} ${isFiltered ? 'blocked' : ''}` }
                onClick = { () => !isFiltered && store.setType('cloudy') }>Облачно</span>
            <span
                className = { `checkbox ${type === 'sunny' ? 'selected' : ''} ${isFiltered ? 'blocked' : ''}` }
                onClick = { () => !isFiltered && store.setType('sunny') }>Солнечно</span>
            <p className = 'custom-input'>
                <label htmlFor = 'min-temperature' >Минимальная температура</label>
                <input
                    id = 'min-temperature'
                    type = 'number'
                    value = { minTemperature }
                    disabled = { isFiltered }
                    onChange = { (event) => store.setMinTemperature(event.target.value) } />
            </p>
            <p className = 'custom-input'>
                <label htmlFor = 'max-temperature'>Максимальная температура</label>
                <input
                    id = 'max-temperature'
                    type = 'number'
                    value = { maxTemperature }
                    disabled = { isFiltered }
                    onChange = { (event) => store.setMaxTemperature(event.target.value) } />
            </p>
            { isFiltered
                ? <button type = 'submit' onClick = { () => store.resetFilter() }>Сбросить</button>
                : <button
                    type = 'submit'
                    disabled = { isFormBlocked }
                    onClick = { () => store.applyFilter({
                        type,
                        minTemperature,
                        maxTemperature,
                    }) }>Отфильтровать</button> }
        </div>
    );
});
