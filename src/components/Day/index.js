import { format } from 'date-fns';
import { ru } from 'date-fns/locale';

import { observer } from 'mobx-react-lite';
import { useStore } from '../../hooks/useStore';


export const Day = observer((props) => {
    const store = useStore();

    const handleClick = () => {
        store.setSelectedDayId(props.id);
    };

    return (
        <div
            className = { `day ${props.type} ${store.selectedDayId === props.id ? 'selected' : ''}`  }
            onClick = { handleClick } >
            <p>{ format(new Date(props.day), 'eeee', { locale: ru }) }</p>
            <span>{ props.temperature }</span>
        </div>
    );
});
