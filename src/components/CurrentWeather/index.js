import { observer } from 'mobx-react-lite';
import { useEffect, useState } from 'react';
import { useStore } from '../../hooks/useStore';
import { useWeather } from '../../hooks/useWeather';

export const CurrentWeather = observer(() => {
    const store = useStore();
    const { selectedDayId } = store;

    const { data } = useWeather();
    const [currentDay, setCurrentDay] = useState();

    useEffect(() => {
        if (selectedDayId) {
            setCurrentDay(data.find((day) => day.id === selectedDayId));
        }
    }, [selectedDayId]);

    return (
        <>
            {
                selectedDayId
                    ? <div className = 'current-weather'>
                        <p className = 'temperature'>{ currentDay?.temperature }</p>
                        <p className = 'meta'>
                            <span className = 'rainy'>% { currentDay?.rain_probability }</span>
                            <span className = 'humidity'>% { currentDay?.humidity }</span>
                        </p>
                    </div>
                    : ''
            }
        </>

    );
});
